<?php

namespace App\Admin\Controllers;

use App\Models\Project;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Admin\Extensions\ExcelExpoter;
class ProjectController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('作品');
            $content->description('顯示');
           
            $content->body($this->grid());
        });


    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('作品');
            $content->description('編輯');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('作品');
            $content->description('新增');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Project::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->name('作品名稱')->sortable();
            $grid->created_at('建立時間')->sortable();
            $grid->updated_at('更新時間')->sortable();

            $grid->filter(function ($filter) {
                // 去掉默认的id过滤器
                $filter->disableIdFilter();
                // 设置created_at字段的范围查询
                //$filter->between('created_at', 'Created Time')->datetime();
                $filter->like('name', '作品名稱');
            });
            

            //$grid->exporter(new ExcelExpoter());

        });

    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Project::class, function (Form $form) {
            $form->display('id', 'ID');
            $form->text('name', 'Name');
            $form->editor('content','Content');
            //$form->editor('body');
            $form->datetime('created_at', '建立時間')->format('YYYY-MM-DD HH:mm:ss');
            $form->datetime('updated_at', '修改時間')->format('YYYY-MM-DD HH:mm:ss');
            $form->image('picture')->uniqueName();

        });
    }
}
